extern crate env_logger;
#[macro_use]
extern crate log;

extern crate actix;
extern crate actix_web;

#[macro_use]
extern crate askama;

extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

use actix_web::{http, server, App};

mod json;
mod render;

fn main() {
    env_logger::init();

    info!("Starting server...");

    let sys = actix::System::new("example");

    server::new(|| {
        App::new()
            .resource("/", |r| r.method(http::Method::GET).f(render::index))
            .resource("/json", |r| r.method(http::Method::GET).with(json::name))
    }).bind("127.0.0.1:8088")
        .unwrap()
        .start();

    info!("Started http server: 127.0.0.1:8088");
    let _ = sys.run();
}
