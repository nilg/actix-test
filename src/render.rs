use askama::Template;

use actix_web::{Error, HttpRequest, HttpResponse};

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate<'a> {
    name: &'a str,
}

pub fn index(_req: HttpRequest) -> Result<HttpResponse, Error> {
    let html = IndexTemplate {
        name: "My name is hello",
    }.render()
        .unwrap();

    Ok(HttpResponse::Ok().content_type("text/html").body(html))
}
