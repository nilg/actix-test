use actix_web::{Query, Error, HttpRequest, HttpResponse, Responder};
use serde_json::to_string;

#[derive(Serialize)]
struct MyObj {
    name: String,
}

#[derive(Deserialize)]
pub struct Info {
    name: String,
}

impl Responder for MyObj {
    type Item = HttpResponse;
    type Error = Error;

    fn respond_to<S>(self, _req: &HttpRequest<S>) -> Result<HttpResponse, Error> {
        let body = to_string(&self)?;

        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(body))
    }
}

pub fn name(info: Query<Info>) -> impl Responder {
    let name = &info.name;
    MyObj { name: name.to_string() }
}
